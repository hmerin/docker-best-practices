# Application development guidelines for workload consolidation infrastructure

## Docker image guidelines

A Docker image consists of read-only layers each of which represents a Dockerfile instruction. The layers are stacked and each one is a delta of the changes from the previous layer.

It is important to write our `Dockerfiles` with few layers as possible to decrease the image weigth and make it faster to trasfer from/to our registry. It could be done using following recommendations:

### Create a .dockerignore file before buildings

1. Create a `.dockerignore` file to exclude all unwanted files from your context, it makes your builds faster.
2. Exclude files that contains passwords, secrets or sensitive data from the context.
3. Exclude temporal directories, .git folder, IDE's configs folders etc.
4. Exclude all non-related with the app files like readmes, contributors etc.
5. Avoid crate heavy layers when `COPY` files.

### Minimize Layers

1. Use necessary layers to build your solutions do not repeat the `RUN`, `COPY`, `ADD` instructions as much as you can.
2. Use [multi line arguments](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#sort-multi-line-arguments) to reduce the `RUN` instructions.
3. Organize your layers from less changing ones to more changing ones to take advantage of cache.

### Minimize image weigth

1. DO NOT install unnecesary packages in the image. Identify the minimal dependencies to run your application.
2. Run commands to install packages without cache. For apt or apk you could run with `--no-cache` flag to avoid, you could make a rm -r to cache directories.
3. Remove packages you don't need after build your app.
4. Remove packages for debug.

### Use multi stage builds

[Multi-stage builds](https://docs.docker.com/develop/develop-images/multistage-build/) allow you to drastically reduce the size of your final image, without struggling to reduce the number of intermediate layers and files.

Because an image is built during the final stage of the build process, you can minimize image layers by leveraging build cache.

1. From your minimized dockerfile identify the build commands for your app.
2. Use this commands as build stage and install the build dependencies only.
3. Copy the built binaries from the build stage to the clean stage and install runtime dependencies your app needs only.

### Add an Healthcheck instruction to your image

The [HEALTHCHECK](https://docs.docker.com/engine/reference/builder/#healthcheck) instruction tells Docker how to test a container to check that it is still working. This can detect cases such as a web server that is stuck in an infinite loop and unable to handle new connections, even though the server process is still running.

When a container has a healthcheck specified, it has a health status in addition to its normal status. This status is initially `starting`. Whenever a health check passes, it becomes `healthy` (whatever state it was previously in). After a certain number of consecutive failures, it becomes `unhealthy`.

### Base Image stability

1. Try to use official images always
2. Always use a fixed version tag for base images e.g.: `nginx:1.17.3-alpine`
3. Use images wich update regulary.

### ENV vs ARG

[ENV](https://docs.docker.com/engine/reference/builder/#env) and [ARG](https://docs.docker.com/engine/reference/builder/#arg) are similar instructions but have different uses. `ENV` must be used when you need to preserve the envvars in the container and this envvars are needed for your app. In the other hand `ARG` must be used to envvars related to the build ecosystem, and them might not necessary for the app itself.

1. Use `ARG` for build requiremets.
2. Use `ENV` for app requirements.

### COPY vs ADD

Although [ADD](https://docs.docker.com/engine/reference/builder/#add) and [COPY](https://docs.docker.com/engine/reference/builder/#copy) are functionally similar, generally speaking, COPY is preferred. That’s because it’s more transparent than ADD. COPY only supports the basic copying of local files into the container, while ADD has some features (like local-only tar extraction and remote URL support) that are not immediately obvious. Consequently, the best use for ADD is local tar file auto-extraction into the image, as in ADD rootfs.tar.xz /.

1. use `ADD` when your source could be downloaded from a http/https public source or need to be extracted.
2. use `COPY` when your source exist in your context only.

### ENTRYPOINT vs CMD

Both [CMD](https://docs.docker.com/engine/reference/builder/#cmd) and [ENTRYPOINT](https://docs.docker.com/engine/reference/builder/#entrypoint) instructions define what command gets executed when running a container. There are few rules that describe their co-operation.

1. Dockerfile should specify at least one of CMD or ENTRYPOINT commands.
2. ENTRYPOINT should be defined when using the container as an executable.
3. CMD should be used as a way of defining default arguments for an ENTRYPOINT command or for executing an ad-hoc command in a container.
4. CMD will be overridden when running the container with alternative arguments.

### Image user

1. Consider create a user with permission to run your application only.

### IMAGE tagging

1. Consider tag your image with `commit sha` from your VCS. It allows you to identify from wich commit your image was built and manage versions easily.

### Avoid using wait-for... scripts

In production clusters you need your containers spawn faster as they can, and a wait-for script just make it slower. We had to supouse that all our `depends_on` services are running and if they don't, your container has to stop and recreate, until `depends_on` services went up.

## Docker container guidelines`

### Container Fast Init

1. Use smaller base images as you can.
2. Package a single app per container.
3. Expose less ports you can.
4. Make an healtcheck endpoint for your app to monitor the app process.
5. Your container has to be able to run as read only and being stateless.

## Security considerations

1. remove debugging aids
2. remove all default passwords
3. remove all keys
4. check and remove for hardcode password or secrets
5. remove extra users/groups
6. use automatic static analizers in your dockerfiles (clair,anchore engine, dagda)

## Application considerations

1. Properly handle PID 1, signal handling, and zombie processes in your app (Gracefully stop)
2. Make you application thinking it could be behind a load balancer
3. Container has to be stateless, all states has to be persisted in a db service
4. Design your app for microservices ecosystem (12-factor-app)
